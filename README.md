# Climate Change

This is a repository for notebooks that showcase climate (change) data. Whenever possible, I will provide a link to a mybinder configured notebook so interested parties can interact with the notebooks.

## Ice Cover

This is a set of notebooks that display the change of the arctic ice cover during summer months. Download the data on your machine. Play with it. [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/KlausGPaul%2Fclimate-change/master?filepath=Arctic%2FIce%2FDownload%20and%20Display%20ice%20images.ipynb)
(Note that I need to find a way to get the data into mybinder as it [blocks ftp](https://github.com/binder-examples/getting-data) .)



![Arctic Sea Ice](http://masie_web.apps.nsidc.org/pub/DATASETS/NOAA/G02135/north/daily/images/2020/09_Sep/N_20200915_extn_blmrbl_v3.0.png)

## Size Comparison of ice cover with known geo features and countries

(under construction)

![ice.cover.1990.europe](https://gitlab.com/KlausGPaul/climate-change/-/raw/master/resources/ice.cover.1990.europe.jpg) ![ice.cover.2020.europe](https://gitlab.com/KlausGPaul/climate-change/-/raw/master/resources/ice.cover.2020.europe.jpg)

